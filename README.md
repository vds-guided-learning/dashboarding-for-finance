# Dashboarding for Finance

**In de “Dashboarding for Finance track” ga je je verdiepen in de Business Analytics service van Microsoft genaamd Power BI en leer je meer over Data Warehouses en Data Lakes. Power BI is de afgelopen jaren uitgegroeid tot een volwaardige dashboard tool die self-service Business Intelligence (BI) mogelijk maakt! Elke maand komt Microsoft met nieuwe features voor Power BI wat er voor zorgt dat de tool over de tijd steeds meer handige features te bieden heeft. In deze training leer je niet alleen hoe Power BI werkt maar leer je ook hoe een zogenoemd “data model” wordt opgezet, deze kennis komt ook goed van pas bij het gebruik van andere dashboard tools.**

## Onderdelen
+ Data: introduction
+ Power BI: introduction (deel 1)
+ Power BI: introduction (deel 2)
+ Use case: department dashboard (deel 1)
+ Power BI: intermediate
+ Use case: department dashboard (deel 2)



